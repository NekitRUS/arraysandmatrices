﻿using System;

namespace Lab1
{
    //Класс работы с одномерными массивами
    static class Vector
    {
        //Вывод одномерного массива на экран
        public static void Show(int[] array)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы массива:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0,5:d}", array[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Нахождение и вывод на экран макс. и мин. элемента в одномерном массиве
        public static void MinMax(int[] array)
        {
            Show(array);
            int max = array[0];
            int min = array[0];
            int imax = 0;
            int imin = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                    imax = i;
                }
                if (array[i] < min)
                {
                    min = array[i];
                    imin = i;
                }
            }
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Максимальный элемент равен {0:d} и находится на {1:d}-й позиции", max, ++imax);
            Console.WriteLine("Минимальный элемент равен {0:d} и находится на {1:d}-й позиции", min, ++imin);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Сортировка одномерного массива
        public static void Sort(int[] array)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы массива до сортировки:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0,5:d}", array[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < array.Length; i++)
            {
                int max = array[i];
                int imax = i;
                for (int j = i; j < array.Length; j++)
                {
                    if (array[j] > max)
                    {
                        max = array[j];
                        imax = j;
                    }
                }
                int temp = array[i];
                array[i] = array[imax];
                array[imax] = temp;
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы массива, отсортированного по убыванию:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0,5:d}", array[i]);
            }
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы массива, отсортированного по возрастанию:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = array.Length - 1; i >= 0; i--)
            {
                Console.Write("{0,5:d}", array[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Создание нового одномерного массива из четных элементов исходного
        public static void EvenArray(int[] array)
        {
            int count = 0;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы исходного массива:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0,5:d}", array[i]);
                if (array[i] % 2 == 0)
                    count++;
            }
            Console.WriteLine();
            int[] NewArray = new int[count];
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Массив из четных элементов исходного:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0, j = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0)
                {
                    NewArray[j] = array[i];
                    Console.Write("{0,5:d}", NewArray[j++]);
                }
            }
            Console.WriteLine();
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
