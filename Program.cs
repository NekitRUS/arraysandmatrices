﻿using System;
using System.IO;

namespace Lab1
{
    class Program
    {
        static void Main()
        {
            int menu;
            const string pathToArray = @"vector.txt";
            const string pathToMatrix = @"matrix.txt";
            const string pathToSecondMatrix = @"secondmatrix.txt";
            const string pathToJagged = @"jagged.txt";
            while (true)
            {
                Console.Clear();
                Console.Title = "Работа с массивами";
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. Ввод с клавиатуры\n2. Ввод из файла\n0. Выход");
                Console.ForegroundColor = ConsoleColor.White;
                try
                {
                    menu = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Повторите ввод");
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                    continue;
                }
                if (menu == 0)
                    break;
                switch (menu)
                {
                    case 1:
                        while (true)
                        {
                            Console.Clear();
                            Console.Title = "Режим ввода данных с клавиатуры";
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("1. Работа с одномерными массивами\n2. Работа с матрицами\n3. Работа со \"ступенчатыми\" массивами\n0. Выход");
                            Console.ForegroundColor = ConsoleColor.White;
                            try
                            {
                                menu = int.Parse(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка! Повторите ввод");
                                Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                Console.ReadKey();
                                continue;
                            }
                            if (menu == 0)
                                break;
                            switch (menu)
                            {
                                case 1:
                                    int[] array;
                                    while (true)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите элементы массива через пробел");
                                        try
                                        {
                                            string[] line = Console.ReadLine().Split(' ');
                                            array = new int[line.Length];
                                            for (int i = 0; i < line.Length; i++)
                                                array[i] = int.Parse(line[i]);
                                            break;
                                        }
                                        catch (Exception)
                                        {
                                            Console.WriteLine("Ошибка! Повторите ввод элементов");
                                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                            Console.ReadKey();
                                            continue;
                                        }
                                    }
                                    Menu.VectorMenu(array);
                                    break;
                                case 2:
                                    int[,] matrix;
                                    int k, m;
                                    while (true)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите размерность матрицы через пробел");
                                        try
                                        {
                                            string[] line = Console.ReadLine().Split(' ');
                                            k = int.Parse(line[0]);
                                            m = int.Parse(line[1]);
                                            matrix = new int[k, m];
                                            Console.WriteLine("Введите элементы матрицы построчно, разделяя элементы пробелами");
                                            for (int i = 0; i < k; i++)
                                            {
                                                String lines = Console.ReadLine();
                                                for (int j = 0; j < m; j++)
                                                    matrix[i, j] = int.Parse(lines.Split(' ')[j]);
                                            }
                                            break;
                                        }
                                        catch (Exception)
                                        {
                                            Console.WriteLine("Ошибка! Повторите ввод элементов");
                                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                            Console.ReadKey();
                                            continue;
                                        }
                                    }
                                    Menu.MatrixMenu(matrix);
                                    break;
                                case 3:
                                    int[][] jagged;
                                    int y;
                                    while (true)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите количество строк массива");
                                        try
                                        {
                                            y = int.Parse(Console.ReadLine());
                                            jagged = new int[y][];
                                            Console.WriteLine("Введите элементы массива построчно, разделяя элементы пробелами");
                                            for (int i = 0; i < y; i++)
                                            {
                                                String lines = Console.ReadLine();
                                                jagged[i] = new int[lines.Split(' ').Length];
                                                for (int j = 0; j < jagged[i].Length; j++)
                                                    jagged[i][j] = int.Parse(lines.Split(' ')[j]);
                                            }
                                            break;
                                        }
                                        catch (Exception)
                                        {
                                            Console.WriteLine("Ошибка! Повторите ввод элементов");
                                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                            Console.ReadKey();
                                            continue;
                                        }
                                    }
                                    Menu.JaggedMenu(jagged);
                                    break;
                            }
                        }
                        break;
                    case 2:
                        while (true)
                        {
                            Console.Clear();
                            Console.Title = "Режим ввода данных из файла";
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("1. Работа с одномерными массивами\n2. Работа с матрицами\n3. Работа со \"ступенчатыми\" массивами\n0. Выход");
                            Console.ForegroundColor = ConsoleColor.White;
                            try
                            {
                                menu = int.Parse(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка! Повторите ввод");
                                Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                Console.ReadKey();
                                continue;
                            }
                            if (menu == 0)
                                break;
                            switch (menu)
                            {
                                case 1:
                                    int[] array;
                                    try
                                    {
                                        StreamReader sr = new StreamReader(pathToArray);
                                        string[] elements = sr.ReadLine().Split(' ');
                                        sr.Close();
                                        array = new int[elements.Length];
                                        for (int i = 0; i < array.Length; i++)
                                            array[i] = int.Parse(elements[i]);
                                        Menu.VectorMenu(array);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Ошибка при открытии файла!\n" + ex.Message);
                                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                        Console.ReadKey();
                                    }
                                    break;
                                case 2:
                                    int[,] matrix;
                                    int lineNumberMatrix = 0;
                                    try
                                    {
                                        StreamReader sr = new StreamReader(pathToMatrix);
                                        while (!sr.EndOfStream)
                                        {
                                            sr.ReadLine();
                                            lineNumberMatrix++;
                                        }
                                        matrix = new int[lineNumberMatrix, lineNumberMatrix];
                                        int count = 0;
                                        sr.BaseStream.Position = 0;
                                        do
                                        {
                                            string[] elements = sr.ReadLine().Split(' ');
                                            for (int i = 0; i < elements.Length; i++)
                                                matrix[count,i] = int.Parse(elements[i]);
                                            count++;
                                        }
                                        while (!sr.EndOfStream);
                                        sr.Close();
                                        Menu.MatrixMenu(matrix, pathToSecondMatrix);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Ошибка при открытии файла!\n" + ex.Message);
                                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                        Console.ReadKey();
                                    }
                                    break;
                                case 3:
                                    int[][] jagged;
                                    int lineNumberJagged = 0;
                                    try
                                    {
                                        StreamReader sr = new StreamReader(pathToJagged);
                                        while (!sr.EndOfStream)
                                        {
                                            sr.ReadLine();
                                            lineNumberJagged++;
                                        }
                                        jagged = new int[lineNumberJagged][];
                                        int count = 0;
                                        sr.BaseStream.Position = 0;
                                        do
                                        {
                                            string[] elements = sr.ReadLine().Split(' ');
                                            jagged[count] = new int[elements.Length];
                                            for (int i = 0; i < elements.Length; i++)
                                                jagged[count][i] = int.Parse(elements[i]);
                                            count++;
                                        }
                                        while (!sr.EndOfStream);
                                        sr.Close();
                                        Menu.JaggedMenu(jagged);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Ошибка при открытии файла!\n" + ex.Message);
                                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                        Console.ReadKey();
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
        }
    }
}
