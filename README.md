Приложение работает с одномерными, двумерными и "ступенчатыми" массивами и выполняет следующие действия:
 
1. Одномерные массивы:
 
    а) без использования класса System.Array:
 
       * вывод элементов массива;
 
       * нахождение макс., мин. элементов;
 
       * прямая и обратная сортировка;
 
       * создание нового массива из четных элементов исходного;
 
     б) с использованием свойств и методов класса System.Array:
 
       * вывод элементов массива;
 
       * нахождение макс., мин. элементов;
 
       * прямая и обратная сортировка;
 
       * создание нового массива из четных элементов исходного;
 
2. Двумерные массивы:
 
       * вывод элементов массива;
 
       * нахождение макс., мин. элементов;
 
       * произведение, сумма, разность двух матриц;
 
3. "Ступенчатые" массивы:
 
       * вывод элементов массива;
 
       * нахождение макс., мин. элементов;
 
       * изменение элементов массива.
 
Предусмотрен ввод данных в массивы:
 
1. с клавиатуры;
 
2. из файла.
 
![Безымянный.png](https://bitbucket.org/repo/yLEr5e/images/559789122-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
 
![Безымянный2.png](https://bitbucket.org/repo/yLEr5e/images/90244167-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B92.png)
 
![Безымянный3.png](https://bitbucket.org/repo/yLEr5e/images/3230354775-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B93.png)
 
![Безымянный4.png](https://bitbucket.org/repo/yLEr5e/images/3671192492-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B94.png)
 
![Безымянный5.png](https://bitbucket.org/repo/yLEr5e/images/3372515359-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B95.png)
 
![Безымянный6.png](https://bitbucket.org/repo/yLEr5e/images/4131822421-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B96.png)
 
![Безымянный7.png](https://bitbucket.org/repo/yLEr5e/images/2715768451-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B97.png)