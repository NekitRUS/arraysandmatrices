﻿using System;

namespace Lab1
{
    class Jagged
    {

        //Вывод на экран элементов ступенчатого массива
        public static void Show(int[][] array)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы массива:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    Console.Write("{0,5:d}", array[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }


        //Поиск и вывод на экран макс. и мин. элементов массива
        public static void MaxMin(int[][] array)
        {
            Show(array);
            int max, min, imax, imin, jmax, jmin;
            max = min = array[0][0];
            imax = imin = jmax = jmin = 0;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    if (array[i][j] > max)
                    {
                        max = array[i][j];
                        imax = i;
                        jmax = j;
                    }
                    if (array[i][j] < min)
                    {
                        min = array[i][j];
                        imin = i;
                        jmin = j;
                    }
                }
            }
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Максимальный элемент равен {0:d} и находится в {1:d}-й строке и {2:d}-м столбце", max, ++imax, ++jmax);
            Console.WriteLine("Минимальный элемент равен {0:d} и находится в {1:d}-й строке и {2:d}-м столбце", min, ++imin, ++jmin);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Изменение элементов массива
        public static void ChangeArray(ref int[][] array)
        {
            int i, j;
            do
            {
                Show(array);
                Console.WriteLine("Введите через пробел номер строки и столбца изменяемого элемента\nДля завершения введите \"0\"");
                try
                {
                    string[] lineRow = Console.ReadLine().Split(' ');
                    if (lineRow[0] == "0")
                        break;
                    i = int.Parse(lineRow[0]);
                    j = int.Parse(lineRow[1]);
                    Console.WriteLine("Введите новое значение элемента [{0:d}][{1:d}]:", i, j);
                    array[--i][--j] = int.Parse(Console.ReadLine());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка! Повторите ввод\n" + ex.Message);
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                }
            }
            while (true);
            Console.Clear();
            Show(array);
        }
    }
}
