﻿using System;
using System.IO;

namespace Lab1
{
    class Matrix
    {
        //Вывод на экран матрицы
        public static void Show(int[,] matrix)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы матрицы:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("{0,5:d}", matrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Нахождение и вывод на экран макс. и мин. элементов матрицы
        public static void MaxMin(int[,] matrix)
        {
            Console.Clear();
            Show(matrix);
            int imax, imin, jmax, jmin;
            imax = imin = jmax = jmin = 0;
            int max = matrix[0, 0];
            int min = matrix[0, 0];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > max)
                    {
                        max = matrix[i, j];
                        imax = i;
                        jmax = j;
                    }
                    if (matrix[i, j] < min)
                    {
                        min = matrix[i, j];
                        imin = i;
                        jmin = j;
                    }
                }
            }
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Максимальный элемент матрицы равен {0:d} и находится в {1:d}-й строке и {2:d}-м столбце", max, ++imax, ++jmax);
            Console.WriteLine("Минимальный элемент матрицы равен {0:d} и находится в {1:d}-й строке и {2:d}-м столбце", min, ++imin, ++jmin);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Вывод на экран суммы, разности и произведения двух матриц
        public static void MatrixMath(int[,] matrixFromFile, string pathToSecond = null)
        {
            int dimension = 0;
            int[,] matrix;
            int[,] secondMatrix = null;
            if (pathToSecond == null)
            {
                while (true)
                {
                    try
                    {
                        Console.Clear();
                        Console.WriteLine("Для выполнения математических операций необходимо ввести размерность квадратной матрицы:");
                        dimension = int.Parse(Console.ReadLine());
                        matrix = new int[dimension, dimension];
                        secondMatrix = new int[dimension, dimension];
                        Console.WriteLine("Введите через пробел элементы матрицы размером {0:d}x{1:d}:", dimension, dimension);
                        string[] elements = Console.ReadLine().Split(' ');
                        for (int i = 0, count = 0; i < dimension; i++)
                        {
                            for (int j = 0; j < dimension; j++)
                            {
                                matrix[i, j] = int.Parse(elements[count++]);
                            }
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ошибка! Повторите ввод матрицы.\n" + ex.Message);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                    }
                }
                while (true)
                {
                    try
                    {
                        Console.Clear();
                        Console.WriteLine("Введите через пробел элементы второй матрицы размером {0:d}x{1:d}:", dimension, dimension);
                        string[] elements = Console.ReadLine().Split(' ');
                        for (int i = 0, count = 0; i < dimension; i++)
                        {
                            for (int j = 0; j < dimension; j++)
                            {
                                secondMatrix[i, j] = int.Parse(elements[count++]);
                            }
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ошибка! Повторите ввод матрицы.\n" + ex.Message);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                    }
                }
            }
            else
            {
                matrix = matrixFromFile;
                try
                {
                    StreamReader sr = new StreamReader(pathToSecond);
                    while (!sr.EndOfStream)
                    {
                        sr.ReadLine();
                        dimension++;
                    }
                    secondMatrix = new int[dimension, dimension];
                    int count = 0;
                    sr.BaseStream.Position = 0;
                    do
                    {
                        string[] elements = sr.ReadLine().Split(' ');
                        for (int i = 0; i < elements.Length; i++)
                            secondMatrix[count, i] = int.Parse(elements[i]);
                        count++;
                    }
                    while (!sr.EndOfStream);
                    sr.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка при открытии файла!\n" + ex.Message);
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                }
            }
            Console.Clear();
            Show(matrix);
            Show(secondMatrix);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы матрицы, полученной в результате сложения исходных:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    Console.Write("{0,5:d}", matrix[i, j] + secondMatrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы матрицы, полученной в результате разности исходных:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    Console.Write("{0,5:d}", matrix[i, j] - secondMatrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Элементы матрицы, полученной в результате произведения исходных:");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    int temp = 0;
                    for (int l = 0; l < dimension; l++)
                    {
                        temp += matrix[i, l] * secondMatrix[l, j];
                    }
                    Console.Write("{0,5:d}", temp);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
