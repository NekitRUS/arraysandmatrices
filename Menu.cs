﻿using System;

namespace Lab1
{
    static class Menu
    {
        public static void VectorMenu(int[] array)
        {
            int menu;
            Console.Clear();
            Console.Title = "Работа с одномерными массивами";
            while (true)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. С использованием System.Array\n2. Без использования System.Array\n0. Выход");
                Console.ForegroundColor = ConsoleColor.White;
                try
                {
                    menu = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Повторите ввод");
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                    continue;
                }
                if (menu == 0)
                    break;
                switch (menu)
                {
                    case 1:
                        while (true)
                        {
                            Console.Clear();
                            Console.Title = "Работа с одномерным массивом с использованием System.Array";
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("1. Вывод элементов массива\n2. Поиск максимального и минимального значений\n3. Сортировка массива\n4. Создание массива из четных элементов\n0. Выход");
                            Console.ForegroundColor = ConsoleColor.White;
                            try
                            {
                                menu = int.Parse(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка! Повторите ввод");
                                Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                Console.ReadKey();
                                continue;
                            }
                            if (menu == 0)
                                break;
                            switch (menu)
                            {
                                case 1:
                                    Console.Clear();
                                    Vector.Show(array);
                                    break;
                                case 2:
                                    Vector.MinMax(array);
                                    break;
                                case 3:
                                    Array.Sort(array);
                                    Vector.Show(array);
                                    break;
                                case 4:
                                    int[] evenArray = Array.FindAll(array, (int n) =>
                                    {
                                        if (n % 2 == 0)
                                            return true;
                                        else
                                            return false;
                                    });
                                    Vector.Show(evenArray);
                                    break;
                            }
                        }
                        break;
                    case 2:
                        while (true)
                        {
                            Console.Clear();
                            Console.Title = "Работа с одномерным массивом без использования System.Array";
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("1. Вывод элементов массива\n2. Поиск максимального и минимального значений\n3. Сортировка массива\n4. Создание массива из четных элементов\n0. Выход");
                            Console.ForegroundColor = ConsoleColor.White;
                            try
                            {
                                menu = int.Parse(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка! Повторите ввод");
                                Console.WriteLine("Для продолжения нажмите любую клавишу...");
                                Console.ReadKey();
                                continue;
                            }
                            if (menu == 0)
                                break;
                            switch (menu)
                            {
                                case 1:
                                    Vector.Show(array);
                                    break;
                                case 2:
                                    Vector.MinMax(array);
                                    break;
                                case 3:
                                    Vector.Sort(array);
                                    break;
                                case 4:
                                    Vector.EvenArray(array);
                                    break;
                            }
                        }
                        break;
                }
            }
        }
        public static void MatrixMenu(int[,] matrix, string pathToSecond = null)
        {
            int menu;
            while (true)
            {
                Console.Clear();
                Console.Title = "Работа с матрицами";
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. Вывод элементов матрицы\n2. Поиск максимального и минимального значений\n3. Математические операции\n0. Выход");
                Console.ForegroundColor = ConsoleColor.White;
                try
                {
                    menu = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Повторите ввод");
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                    continue;
                }
                if (menu == 0)
                    break;
                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        Matrix.Show(matrix);
                        break;
                    case 2:
                        Matrix.MaxMin(matrix);
                        break;
                    case 3:
                        Matrix.MatrixMath(matrix, pathToSecond);
                        break;
                }
            }
        }
        public static void JaggedMenu(int[][] jagged)
        {
            int menu;
            while (true)
            {
                Console.Clear();
                Console.Title = "Работа со \"ступенчатыми\" массивами";
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1. Вывод элементов массива\n2. Поиск максимального и минимального значений\n3. Замена элементов\n0. Выход");
                Console.ForegroundColor = ConsoleColor.White;
                try
                {
                    menu = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Повторите ввод");
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                    continue;
                }
                if (menu == 0)
                    break;
                switch (menu)
                {
                    case 1:
                        Jagged.Show(jagged);
                        break;
                    case 2:
                        Jagged.MaxMin(jagged);
                        break;
                    case 3:
                        Jagged.ChangeArray(ref jagged);
                        break;
                }
            }
        }
    }
}
